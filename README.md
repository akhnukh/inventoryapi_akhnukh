# Inventory API Test Assignment #

This is an implementation of Spring Boot REST service implmentation based on attached InventoryAPI specification.


### Version ###
InventoryAPI is implemented using: 

* Java  			1.8.0_251
* Spring Boot  		2.3.1
* Apache Maven 		3.6.3
* Database			h2

### How to set up? ###

* Download or checkout all files/folder to local file system. i.e **C:/API/**
* Open Command Prompt.
* Change path to **C:/API/**
* Run **startInventoryAPI.cmd**

### How to test API ###

Attached Postman collection can be used to test API, [InventoryApi Postman Collection](testing/inventoryApiPostmanCollection).


### Reference ###

API Specification: Specification can be found in requirements folder [InventoryAPI Specs](requirements/inventory-api-spec.yml)