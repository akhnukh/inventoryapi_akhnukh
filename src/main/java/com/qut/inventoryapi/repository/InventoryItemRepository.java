package com.qut.inventoryapi.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.qut.inventoryapi.model.InventoryItem;

@Repository
public interface InventoryItemRepository extends CrudRepository<InventoryItem, String>  {

}
