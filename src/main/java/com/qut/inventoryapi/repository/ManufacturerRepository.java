package com.qut.inventoryapi.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.qut.inventoryapi.model.Manufacturer;

@Repository
public interface ManufacturerRepository extends CrudRepository<Manufacturer, String>  {

}
