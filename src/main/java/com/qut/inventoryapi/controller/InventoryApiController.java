package com.qut.inventoryapi.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.qut.inventoryapi.common.CommonCodes;
import com.qut.inventoryapi.model.InventoryItem;
import com.qut.inventoryapi.service.InventoryApiService;

@RestController
public class InventoryApiController {
    private static final Logger log = LoggerFactory.getLogger(InventoryApiController.class);

    private final HttpServletRequest request;

    @Autowired
    InventoryApiService inventoryApiService;

    @Autowired
    InventoryApiController(HttpServletRequest request) {
        this.request = request;
    }

   	public static Logger getLog() {
		return log;
	}


	public HttpServletRequest getRequest() {
		return request;
	}


	@RequestMapping(value = "/inventory", method = RequestMethod.GET)
    public ResponseEntity<List<InventoryItem>> getAllInventoryItems(@Valid @RequestParam(value = "skip", required = false) Integer skip, @Valid @RequestParam(value = "limit", required = false ) Integer limit) {
        String accept = request.getHeader("Accept");
        limit = (limit==null)?-1:limit;
        skip = (skip==null)?0:skip;
        
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<InventoryItem>>(inventoryApiService.getAllInventoryItems(limit,skip), HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<InventoryItem>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<InventoryItem>>(HttpStatus.NOT_IMPLEMENTED);
    }

    @RequestMapping(value = "/inventory", method = RequestMethod.POST)
    public ResponseEntity<Void> saveInventoryItem( @Valid @RequestBody InventoryItem inventoryItem) {
    	int addResultCode = inventoryApiService.addInventoryItem(inventoryItem);
    	
		ResponseEntity<Void> responseEntity = new ResponseEntity<Void>(HttpStatus.OK);

    	if (addResultCode == CommonCodes.ITEM_CREATED_CODE)
    		responseEntity = new ResponseEntity<Void>(HttpStatus.CREATED);
    	else if (addResultCode == CommonCodes.ITEM_EXISTS_CODE)
    		responseEntity = new ResponseEntity<Void>(HttpStatus.CONFLICT);
    	else if (addResultCode == CommonCodes.ITEM_INVALID_INPUT_CODE)
    		responseEntity = new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
    	
        return responseEntity;
    }

    @RequestMapping(value = "/inventory/{id}", method = RequestMethod.GET)
    public ResponseEntity<InventoryItem> getInventoryItemById(@Valid @PathVariable("id") String id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
            	Optional<InventoryItem> result = inventoryApiService.getInventoryItemById(id);
				
            	ResponseEntity<InventoryItem> responseEntity =  new ResponseEntity<InventoryItem>(HttpStatus.NOT_FOUND);

            	if(result.isPresent())
    				responseEntity = new ResponseEntity<InventoryItem>(result.get(), HttpStatus.OK);
    			
                return responseEntity;
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<InventoryItem>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<InventoryItem>(HttpStatus.NOT_IMPLEMENTED);
    }
}
