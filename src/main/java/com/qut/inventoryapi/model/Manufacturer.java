package com.qut.inventoryapi.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

@Entity
public class Manufacturer {
	
	@Id
	@NotEmpty(message="invalid input, object invalid")
	private String name;
	
	private String homepage;
	
	private String phone;
	
	public Manufacturer() {
		super();
	}

	public Manufacturer(String name, String homepage, String phone) {
		super();
		this.name = name;
		this.homepage = homepage;
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Manufacturer [name=" + name + ", homepage=" + homepage + ", phone=" + phone + "]";
	}
	
}
