package com.qut.inventoryapi.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;


@Entity
public class InventoryItem {
	
	@Id
	@NotEmpty(message="invalid input, object invalid")
	private String id;
	
	@NotEmpty(message="invalid input, object invalid")
	private String name;
	
	@NotEmpty(message="invalid input, object invalid")
	private String releaseDate;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="manufacturer_name", referencedColumnName="name")
	private Manufacturer manufacturer;


	public InventoryItem() {
		super();
	}


	public InventoryItem(String id, String name, String releaseDate, Manufacturer manufacturer) {
		super();
		this.id = id;
		this.name = name;
		this.releaseDate = releaseDate;
		this.manufacturer = manufacturer;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getReleaseDate() {
		return releaseDate;
	}


	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}


	public Manufacturer getManufacturer() {
		return manufacturer;
	}


	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}


	@Override
	public String toString() {
		return "InventoryItem [id=" + id + ", name=" + name + ", releaseDate=" + releaseDate + ", manufacturer="
				+ manufacturer + "]";
	}
	
}
