package com.qut.inventoryapi.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qut.inventoryapi.common.CommonCodes;
import com.qut.inventoryapi.model.InventoryItem;
import com.qut.inventoryapi.repository.InventoryItemRepository;



@Service
public class InventoryApiService {
	@Autowired
	InventoryItemRepository inventoryItemRepository;
	
	public InventoryItemRepository getInventoryItemRepository() {
		return inventoryItemRepository;
	}

	public void setInventoryItemRepository(InventoryItemRepository inventoryItemRepository) {
		this.inventoryItemRepository = inventoryItemRepository;
	}

	public List<InventoryItem> getAllInventoryItems(long limit, long skip) {
		
		Iterable<InventoryItem> invetoryIterable = inventoryItemRepository.findAll();

		Stream<InventoryItem> stream = StreamSupport.stream(invetoryIterable.spliterator(), false);

		
		if(limit!=-1) 
			stream = stream.limit(limit);
		
		if(skip>0)
			stream = stream.skip(skip);
		
		List<InventoryItem> result = stream.collect(Collectors.toList());
	    
		return result;
	}
	
	public Optional<InventoryItem> getInventoryItemById(String itemId) {
		
		Optional<InventoryItem> item = inventoryItemRepository.findById(itemId);
		
		return item;
	}

	public InventoryItem getInventoryItemObjectById(UUID id) {
	
		Optional<InventoryItem> result = getInventoryItemById(id.toString());
		
		return result.isPresent()?result.get():null;
	}
	public int addInventoryItem(InventoryItem inventoryItem) {
		
		
		if(getInventoryItemById(inventoryItem.getId()).isPresent())
			return CommonCodes.ITEM_EXISTS_CODE;
		
		int returnCode=CommonCodes.ITEM_CREATED_CODE;
		try {
			inventoryItemRepository.save(inventoryItem);
		}catch(Exception e) {
			returnCode = CommonCodes.ITEM_INVALID_INPUT_CODE;
		}
		
		return returnCode;
	}
}
