package com.qut.inventoryapi.common;

public class CommonCodes {
	public final static int ITEM_FOUND=200;
	public final static int ITEM_CREATED_CODE=201;
	public final static int ITEM_EXISTS_CODE=409;
	public final static int ITEM_INVALID_INPUT_CODE=400;
}
