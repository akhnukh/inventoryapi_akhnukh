package com.qut.inventoryapi.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.qut.inventoryapi.common.CommonCodes;
import com.qut.inventoryapi.model.InventoryItem;
import com.qut.inventoryapi.model.Manufacturer;
import com.qut.inventoryapi.repository.InventoryItemRepository;
import com.qut.inventoryapi.repository.ManufacturerRepository;

@RunWith(MockitoJUnitRunner.class)
public class InventoryApiServiceTest {
	
	@Mock
	InventoryItemRepository inventoryItemRepository;
	
	@Mock
	ManufacturerRepository manufacturerRepository;
	
	@InjectMocks
	InventoryApiService inventoryApiService;
	  
	  
	@Test
	public void testPositiveGetInventoryItems() {

		Manufacturer lgManfac = new Manufacturer("LG", "www.lg.com", "+6112312312");
		InventoryItem inventoryItem = new InventoryItem("4f152e50-37d0-4951-83fa-2798f4faca8a", "HD TV", "12-01-2020", lgManfac);
		List<InventoryItem> inventoryItems = new ArrayList<InventoryItem>();
		inventoryItems.add(inventoryItem);
	}
	
	@Test
	public void testPositiveGetInventoryItemWithLimit() {

		Manufacturer lgManfac = new Manufacturer("LG", "www.lg.com", "+6112312312");
		List<InventoryItem> inventoryItems = new ArrayList<InventoryItem>();
		inventoryItems.add(new InventoryItem(UUID.randomUUID().toString(), "HD TV", "12-01-2020", lgManfac));
		inventoryItems.add(new InventoryItem(UUID.randomUUID().toString(), "Microwave", "12-01-2017", lgManfac));
		inventoryItems.add(new InventoryItem(UUID.randomUUID().toString(), "Fridge", "12-01-2018", lgManfac));
		
		Collection<InventoryItem> collection = inventoryItems;
	    Iterable<InventoryItem> iterable = collection;
	    when(inventoryItemRepository.findAll()).thenReturn(iterable);
		
	    assertEquals(3, inventoryApiService.getAllInventoryItems(3,0).size());
		
	}
	
	@Test
	public void testPositiveGetInventoryItemWithSkip() {

		Manufacturer lgManfac = new Manufacturer("LG", "www.lg.com", "+6112312312");
		List<InventoryItem> inventoryItems = new ArrayList<InventoryItem>();
		inventoryItems.add(new InventoryItem(UUID.randomUUID().toString(), "HD TV", "12-01-2020", lgManfac));
		inventoryItems.add(new InventoryItem(UUID.randomUUID().toString(), "Microwave", "12-01-2017", lgManfac));
		inventoryItems.add(new InventoryItem(UUID.randomUUID().toString(), "Fridge", "12-01-2018", lgManfac));
		
		Collection<InventoryItem> collection = inventoryItems;
	    Iterable<InventoryItem> iterable = collection;
	    
		when(inventoryItemRepository.findAll()).thenReturn(iterable);

		assertEquals(2, inventoryApiService.getAllInventoryItems(-1,1).size());
		
	}
	
	@Test
	public void testPositiveGetInventoryItemById() {
		
		Manufacturer lgManfac = new Manufacturer("LG", "www.lg.com", "+6112312312");
		InventoryItem inventoryItem = new InventoryItem(UUID.randomUUID().toString(), "HD TV", "12-01-2020", lgManfac);

		Optional<InventoryItem> result = Optional.of(inventoryItem);
	
		when(inventoryItemRepository.findById(Mockito.anyString())).thenReturn(result);

		assertTrue(inventoryApiService.getInventoryItemById("bc28aa7c-5f9c-490c-ab01-dfd93c1dbd25").isPresent());
	}
	
	@Test
	public void testNegativeGetInventoryItemById() {

		Optional<InventoryItem> result = Optional.empty();
	
		when(inventoryItemRepository.findById(Mockito.anyString())).thenReturn(result);

		assertFalse(inventoryApiService.getInventoryItemById("bc28aa7c-5f9c-490c-ab01-dfd93c1dbd2").isPresent());

	}
	
	@Test
	public void testPositiveAddInventoryItem() {
		
		Manufacturer lgManfac = new Manufacturer("LG", "www.lg.com", "+6112312312");
		InventoryItem inventoryItem = new InventoryItem(UUID.randomUUID().toString(), "HD TV", "12-01-2020", lgManfac);

		when(inventoryItemRepository.save(Mockito.any())).thenReturn(Mockito.any());

		assertEquals(CommonCodes.ITEM_CREATED_CODE, inventoryApiService.addInventoryItem(inventoryItem));
	}
	
	@Test
	public void testNegativeAddDuplicateInventoryItem() {
		
		Manufacturer lgManfac = new Manufacturer("LG", "www.lg.com", "+6112312312");
		InventoryItem inventoryItem = new InventoryItem(UUID.randomUUID().toString(), "HD TV", "12-01-2020", lgManfac);
		
		Optional<InventoryItem> optional = Optional.of(inventoryItem);

		when(inventoryItemRepository.findById(Mockito.any())).thenReturn(optional);

		assertEquals(CommonCodes.ITEM_EXISTS_CODE, inventoryApiService.addInventoryItem(inventoryItem));
	}


}
