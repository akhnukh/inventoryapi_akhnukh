package com.qut.inventoryapi.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParseException;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qut.inventoryapi.common.CommonCodes;
import com.qut.inventoryapi.model.InventoryItem;
import com.qut.inventoryapi.model.Manufacturer;
import com.qut.inventoryapi.service.InventoryApiService;


@RunWith(SpringRunner.class)
@WebMvcTest(value = InventoryApiController.class)

public class InventoryApiControllerTest {

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private InventoryApiService inventoryApiService;

	

	protected String mapToJson(Object obj) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(obj);
	}

	private <T> T mapFromJson(String json, Class<T> clazz)
			throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, clazz);
	}

	@Test
	public void testGetAllInventoryItems() throws Exception {
		
		String uri = "/inventory";

		Manufacturer lgManfac = new Manufacturer("LG", "www.lg.com", "+6112312312");
		List<InventoryItem> inventoryItems = new ArrayList<InventoryItem>();
		inventoryItems.add(new InventoryItem(UUID.randomUUID().toString(), "HD TV", "12-01-2020", lgManfac));
		inventoryItems.add(new InventoryItem(UUID.randomUUID().toString(), "Microwave", "12-01-2017", lgManfac));
		inventoryItems.add(new InventoryItem(UUID.randomUUID().toString(), "Fridge", "12-01-2018", lgManfac));

		Mockito.when(
				inventoryApiService.getAllInventoryItems(Mockito.anyLong(),Mockito.anyLong())).thenReturn(inventoryItems);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(
				MediaType.APPLICATION_JSON);
		
		MvcResult result = mvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse());

		int status = result.getResponse().getStatus();
		assertEquals(CommonCodes.ITEM_FOUND, status);
		
		String content = result.getResponse().getContentAsString();
		InventoryItem[] inventoryList = mapFromJson(content, InventoryItem[].class);
		assertTrue(inventoryList.length > 0);
	}
	
	@Test
	public void testGetAllInventoryItemsWithLimit() throws Exception {
		
		String uri = "/inventory?limit=2";

		Manufacturer lgManfac = new Manufacturer("LG", "www.lg.com", "+6112312312");
		List<InventoryItem> inventoryItems = new ArrayList<InventoryItem>();
		inventoryItems.add(new InventoryItem(UUID.randomUUID().toString(), "HD TV", "12-01-2020", lgManfac));
		inventoryItems.add(new InventoryItem(UUID.randomUUID().toString(), "Microwave", "12-01-2017", lgManfac));
	
		Mockito.when(
				inventoryApiService.getAllInventoryItems(Mockito.anyLong(),Mockito.anyLong())).thenReturn(inventoryItems);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(
				MediaType.APPLICATION_JSON);
		
		MvcResult result = mvc.perform(requestBuilder).andReturn();

		int status = result.getResponse().getStatus();
		assertEquals(CommonCodes.ITEM_FOUND, status);
		
		String content = result.getResponse().getContentAsString();
		InventoryItem[] inventoryList = mapFromJson(content, InventoryItem[].class);
		assertEquals(inventoryList.length,2);
	}
	
	@Test
	public void testGetAllInventoryItemsWithSkip() throws Exception {
		
		String uri = "/inventory?skip=1";

		Manufacturer lgManfac = new Manufacturer("LG", "www.lg.com", "+6112312312");
		List<InventoryItem> inventoryItems = new ArrayList<InventoryItem>();
		inventoryItems.add(new InventoryItem(UUID.randomUUID().toString(), "HD TV", "12-01-2020", lgManfac));
	
		Mockito.when(
				inventoryApiService.getAllInventoryItems(Mockito.anyLong(),Mockito.anyLong())).thenReturn(inventoryItems);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(
				MediaType.APPLICATION_JSON);
		
		MvcResult result = mvc.perform(requestBuilder).andReturn();

		int status = result.getResponse().getStatus();
		assertEquals(CommonCodes.ITEM_FOUND, status);
		
		String content = result.getResponse().getContentAsString();
		InventoryItem[] inventoryList = mapFromJson(content, InventoryItem[].class);
		assertEquals(inventoryList.length,1);
	}
	
	@Test
	public void testGetAllInventoryItemsBadRequest() throws Exception {
		
		String uri = "/inventory?skip=abc";

		Manufacturer lgManfac = new Manufacturer("LG", "www.lg.com", "+6112312312");
		List<InventoryItem> inventoryItems = new ArrayList<InventoryItem>();
		inventoryItems.add(new InventoryItem(UUID.randomUUID().toString(), "HD TV", "12-01-2020", lgManfac));
	
		Mockito.when(
				inventoryApiService.getAllInventoryItems(Mockito.anyLong(),Mockito.anyLong())).thenReturn(inventoryItems);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(
				MediaType.APPLICATION_JSON);
		
		MvcResult result = mvc.perform(requestBuilder).andReturn();

		int status = result.getResponse().getStatus();
		assertEquals(CommonCodes.ITEM_INVALID_INPUT_CODE, status);
	}

	@Test
	public void testPositiveSaveInventoryItem() throws Exception {
		
		String uri = "/inventory";
		Manufacturer lgManfac = new Manufacturer("LG", "www.lg.com", "+6112312312");
		String inputContent = mapToJson(new InventoryItem(UUID.randomUUID().toString(), "HD TV", "12-01-2020", lgManfac));
		
		Mockito.when(
				inventoryApiService.addInventoryItem(Mockito.any())).thenReturn(CommonCodes.ITEM_CREATED_CODE);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(uri).
				content(inputContent).
				contentType(MediaType.APPLICATION_JSON_VALUE).
				accept(MediaType.APPLICATION_JSON_VALUE);
		
		MvcResult result = mvc.perform(requestBuilder).andReturn();
		int status = result.getResponse().getStatus();
		
		assertEquals(CommonCodes.ITEM_CREATED_CODE ,status);
	}
	
	@Test
	public void testPositiveFindById() throws Exception {
		
		String uri = "/inventory/106f7675-47fd-4d53-bca4-7e8937056e58";

		Manufacturer lgManfac = new Manufacturer("LG", "www.lg.com", "+6112312312");
		InventoryItem inventoryItem = new InventoryItem(UUID.randomUUID().toString(), "HD TV", "12-01-2020", lgManfac);
		Optional<InventoryItem> mockResult = Optional.of(inventoryItem);
		
		Mockito.when(
				inventoryApiService.getInventoryItemById(Mockito.anyString())).thenReturn(mockResult);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(
				MediaType.APPLICATION_JSON);
		
		MvcResult result = mvc.perform(requestBuilder).andReturn();
		int status = result.getResponse().getStatus();
		
		assertEquals(CommonCodes.ITEM_FOUND ,status);
	}

}
